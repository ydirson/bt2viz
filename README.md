# Visualization tools for babeltrace2

This project is in the early prototyping phase.

Overall goals:
- provide visualization tools akin to what is provided by
  TraceCompass, using the babeltrace2 component architecture rather
  than Eclipse components
- provide tools to use lttng traces for profiling of a system, and
  specifically to help in analyzing CPU-saturating situations

This will be done using Python for now, for ease of prototyping.  The
GUI will use Qt/QML, prototyping with QtCharts.

## envisionned steps

- visualization
  - [x] get a first Qt UI ~~acting as a bt2 sink~~, simply presenting event
        data in a table
  - [ ] ~~add a preliminary plot of per-thread events, using a
        QtCharts ScatterSeries~~
  - [ ] design a suitable display widget ~~somewhere under QtCharts
        AbstractBarSeries~~
  - [ ] a widget to visualize time slices and sub-slices
  - [ ] a widget to show percentage of CPU usage in interval, with
        decomposition in slice types (syscall types, user-slice types, ...)
- tooling
  - [ ] a bt2 filter to derive process/thread information (CPU slices,
        syscall sub-slices, stats on execution time, ...) from a lttng
        event stream
  - [ ] a bt2 filter to help decomposing user run time into sub-slices
        for finer analysis

## lessons learned to date

- QtCharts will most probably not scale for timelines.  Too many
  events to manage, and not using Flickable is probably the main
  reason.  Also does not use the MVD system so needs its own copy of
  the data in a different data structure (there are *ModelMapper
  classes, but all they do is keeping in 2-way sink the model data and
  the series data, doubly wasteful).
