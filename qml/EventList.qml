import QtQuick 2.6
import QtQuick.Controls 2.15

ScrollView {
    clip: true
    ScrollBar.vertical.policy: ScrollBar.AlwaysOn

    function append_event(stamp, evttype, contents) {
        var contentlist = []
        for (var key in contents) {
            contentlist.push(key + "=" + contents[key])
        }
        event_model.append({ tstamp: stamp, type: evttype,
                             contents: contentlist.join(", ") })

//        event_model.append({ tstamp: stamp, type: evttype,
//                             contents: contents.toString() })
    }

    ListView {
        id: event_view
        anchors.fill: parent
        spacing: -1

        property int width_ts: 200
        property int width_evtype: 300
        property int row_height: 20

        model: ListModel {
            id: event_model
        }

        header: Component {
            Row {
                id: event_row
                property int text_padding: 5
                Rectangle {
                    width: event_view.width_ts
                    height: event_view.row_height + 2 * event_row.text_padding
                    Text {
                        text: "Timestamp"
                        font.bold: true
                        padding: event_row.text_padding
                    }
                }
                Rectangle {
                    width: event_view.width_evtype
                    height: event_view.row_height + 2 * event_row.text_padding
                    Text {
                        text: "Event type"
                        font.bold: true
                        padding: event_row.text_padding
                    }
                }
                Rectangle {
                    width: 500
                    height: event_view.row_height + 2 * event_row.text_padding
                    Text {
                        text: "Contents"
                        font.bold: true
                        padding: event_row.text_padding
                    }
                }
            }
        }
        delegate: Component {
            Row {
                Rectangle {
                    width: event_view.width_ts
                    height: event_view.row_height
                    Text {
                        text: tstamp
                        padding: 1
                    }
                }
                Rectangle {
                    width: event_view.width_evtype
                    height: event_view.row_height
                    Text {
                        text: type
                        padding: 1
                    }
                }
                Rectangle {
                    width: 500
                    height: event_view.row_height
                    Text {
                        text: contents
                        padding: 1
                    }
                }
            }
        }
    }
}
