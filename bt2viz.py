#! /usr/bin/env python3

import bt2
import datetime
import sys
import time
from pathlib import Path

# Although PySide provides some appealing modern features, PyQt5
# usually has much better error reporting.  Though PySide2 fails to pass
# big ints to qml, and PyQt5 rather truncates them.
if False:
    from PySide2 import QtCore, QtGui
    from PySide2.QtQuick import QQuickView
    from PySide2.QtCore import QObject, QUrl
    QtSignal = QtCore.Signal
else:
    from PyQt5 import QtCore, QtGui
    from PyQt5.QtQuick import QQuickView
    from PyQt5.QtCore import QObject, QUrl
    QtSignal = QtCore.pyqtSignal


class BabeltraceReader(QtCore.QThread):
    # FIXME QDateTime truncates to ms
    new_event = QtSignal(QtCore.QDateTime, str, dict)
    finished = QtSignal()

    def __init__(self, filename, parent = None):
        super().__init__(parent)
        self._filename = filename
        self.__exiting = False

        self.__event_count = 0
        self.__start_time = 0
        self.__unhandled_datatypes = set()

    def __del__(self):
        self.__exiting = True
        #self.wait() # FIXME causes "wrapped C/C++ object of type
        #            # BabeltraceReader has been deleted" in PyQt5

    def run(self):
        msg_it = bt2.TraceCollectionMessageIterator(self._filename)
        self.__start_time = time.time()
        for msg in msg_it:
            self.__event_count += 1
            if self.__exiting:
                print("aborting import", file=sys.stderr)
                return
            self.__handle_message(msg)
        elapsed = time.time() - self.__start_time
        print(f"{self.__event_count} events imported in {elapsed}sec, {self.__event_count/elapsed} ev/sec.", file=sys.stderr)
        self.finished.emit()

    # map bt2 types to python types, so Qml can itself map to its own
    def to_python(self, bt2_value):
        if ( type(bt2_value) is bt2._UnsignedIntegerFieldConst
             or type(bt2_value) is bt2._SignedIntegerFieldConst):
            return int(bt2_value)

        if type(bt2_value) is bt2._StringFieldConst:
            return str(bt2_value)

        if ( type(bt2_value) is bt2._UnsignedEnumerationFieldConst
             or type(bt2_value) is bt2._SignedEnumerationFieldConst):
            return str(bt2_value) # includes value and label(s?)

#        if hasattr(bt2_value, "_value"):
#            return bt2_value._value # FIXME internal API

        if ( type(bt2_value) is bt2._DynamicArrayFieldConst
             or type(bt2_value) is bt2._StaticArrayFieldConst):
            return [self.to_python(i) for i in bt2_value]

        if type(bt2_value) is bt2._StructureFieldConst:
            return {k: self.to_python(v) for k, v in bt2_value.items()}

        # unhandled
        if type(bt2_value) not in self.__unhandled_datatypes:
            print(f"unhandled datatype {type(bt2_value)}: {bt2_value}", file=sys.stderr)
            self.__unhandled_datatypes.add(type(bt2_value))
        return f"{type(bt2_value)}()"

    def __handle_message(self, msg):
        if type(msg) is not bt2._EventMessageConst:
            return

        # FIXME what of specific_context_field and common_context_field ?
        ts = datetime.datetime.fromtimestamp(msg.default_clock_snapshot.ns_from_origin / 1e9)
        self.new_event.emit(ts,
                            msg.event.name,
                            self.to_python(msg.event.payload_field),
                            )

class Viz(object):
    def __init__(self):
        self.__app_start_time = time.time()
        self._app = QtGui.QGuiApplication(sys.argv)
        self._filename = sys.argv[1]
        self._app.setApplicationDisplayName(f"Event visualizer - {self._filename}")
        self._view = QQuickView()
        self._view.setResizeMode(QQuickView.SizeRootObjectToView)
        self._view.resize(800,600)

        qml_file = Path(__file__).parent / "qml" / "bt2viz.qml"
        self._view.setSource(QUrl.fromLocalFile(str(qml_file.resolve())))
        if self._view.status() == QQuickView.Error:
            sys.exit(-1)

        self._reader = BabeltraceReader(self._filename)

        root = self._view.rootObject()
        self._eventView = root.findChild(QObject, "event_list")
        self._reader.new_event.connect(self._eventView.append_event)
        self._reader.finished.connect(self.onReaderFinished)

    def run(self):
        self._reader.start()

        self._view.show()
        self._app.exec_()
        del self._view

    def onReaderFinished(self):
        elapsed = time.time() - self.__app_start_time
        print(f"App started in {elapsed}sec.")

if __name__ == "__main__":
    v = Viz()
    v.run()

